require('dotenv').config();
const axios = require('axios');
const fs = require('fs').promises;

const TelegramBot = require('node-telegram-bot-api');
const chatId = 5597012;


const urlApi = 'https://www.bosi.com.co/api/catalog_system/pub/products/variations/1071';
let oldPrice = 0;

const token = '6987154159:AAFIvR2JoOMxIUg7uitVg56PCy41V-SOU1I';

console.log("El script se ha ejecutado correctamente.");


const bot = new TelegramBot(token, { polling: true });

async function verifyPriceAndSendNotification() {
    try {
        // Verificar si el archivo existe utilizando fs.access
        try {
            await fs.access('precio_anterior.txt');
        } catch (error) {
            // Si no existe, crearlo con el valor inicial '0'
            await fs.writeFile('precio_anterior.txt', '0');
        }

        const data = await fs.readFile('precio_anterior.txt', 'utf8');
        oldPrice = parseInt(data);

        const { data: responseData } = await axios.get(urlApi);
        const bestPrice = responseData.skus.find(sku => sku.available).bestPrice;
        const product = {
            name: responseData.name,
            image: responseData.skus.find(sku => sku.available).image
        };

        console.log(product.image);

        let notificationMessage = '';
        if (oldPrice !== null && oldPrice !== bestPrice) {
            console.log('Precio ha cambiado', formatPrice(oldPrice), '->', formatPrice(bestPrice));
            notificationMessage = `🚨🚨🚨El precio ha cambiado de ${formatPrice(oldPrice)} a ${formatPrice(bestPrice)} 📉📉📉`;
        } else {
            console.log('Precio no ha cambiado', formatPrice(bestPrice));
            notificationMessage = `❌❌❌su precio no ha cambiado, sigue en ${formatPrice(bestPrice)} 🙎🙎🙎`;
            console.log("PASO")
        }

        // Enviar notificación
        await sendNotification(notificationMessage, product);

        // Actualizar el archivo con el nuevo precio
        await fs.writeFile('precio_anterior.txt', bestPrice.toString(), 'utf8');

        // Finalizar el proceso después de enviar la notificación
        process.exit();
    } catch (error) {
        console.error('Error:', error);
        // Si hay un error, también se puede salir del proceso
        process.exit(1);
    }
}

function formatPrice(price) {
    return new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP',
    }).format(price / 100);
}

async function sendNotification(message, product) {
    try {
        await bot.sendPhoto(chatId, product.image, { caption: `El producto ${product.name} \n ${message}` });
    } catch (error) {
        console.error('Error al enviar la notificación:', error);
        throw error;
    }
}

verifyPriceAndSendNotification();